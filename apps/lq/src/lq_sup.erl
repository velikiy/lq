%%%-------------------------------------------------------------------
%% @doc lq top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('lq_sup').

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    T= {lq_t, {lq_t, start_link, [1]}, permanent, 5000, worker, [lq_t]},
    R= {lq_r, {lq_r, start_link, [2]}, permanent, 5000, worker, [lq_r]},
    W= {lq_w, {lq_w, start_link, [3]}, permanent, 5000, worker, [lq_w]},
    _In= {lq_in, {lq_in, start_link, [4]}, permanent, 5000, worker, [lq_in]},
    {ok, { {one_for_one, 1, 1}, [T, R, W]} }.

%%====================================================================
%% Internal functions
%%====================================================================
