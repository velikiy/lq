%%% @author   Andrey Velikiy <velikiy@glosav.ru>
%%% @copyright  2015  Andrey Velikiy.
%%% @doc      

-module(lq_t).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

-author('Andrey Velikiy <velikiy@glosav.ru>').
-include_lib("stdlib/include/ms_transform.hrl").

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------
-export([start_link/1]).


%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------
-export([init/1, handle_call/3, handle_cast/2, terminate/2, handle_info/2, code_change/3]).


%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------
start_link(_) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------
init(Args) ->
    {ok, _TRef}=timer:send_interval(1000, second),
    %% TODO init RMQ conn
    {ok, Args}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(second, State) ->
    T=erlang:system_time(seconds),
    F1=ets:fun2ms(fun({{P, Id}, T1, true}) when T1==T -> {P, Id, T} end),
    F2=ets:fun2ms(fun({{P, Id}, T1, _}) when T1<T -> true end),
    L=ets:select(tasks, F1),
    %% TODO handle found tuples
    F=fun({_P, _Id, _T}) ->
        %% TODO queue task to RMQ
        ok;
      ({_, _, _}) ->
        %% do nothing
        ok
    end,
    lists:foreach(F, L),
    case L of
      [] -> ok;
      _ -> lager:info("SELECT: ~p", [L])
    end,
    _Num=ets:select_delete(tasks, F2),
    {noreply, State};

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------
