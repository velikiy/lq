%%% @author   Andrey Velikiy <velikiy@glosav.ru>
%%% @copyright  2015  Andrey Velikiy.
%%% @doc      

-module(lq_in).
-behaviour(gen_server).
-define(SERVER, ?MODULE).
-define(MAXTMEM, 10000000000000).

-author('Andrey Velikiy <velikiy@glosav.ru>').

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------
-export([start_link/1]).
-export([add/3, del/2]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------
-export([init/1, handle_call/3, handle_cast/2, terminate/2, handle_info/2, code_change/3]).


%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------
start_link(_) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

add(P, Id, _DT) when not is_integer(P); not is_binary(Id) -> error;

add(P, Id, {D, T}) ->
    T1=calendar:datetime_to_gregorian_seconds({D, T})-62167219200,
    T2=erlang:system_time(seconds),
    add(P, Id, T1-T2),
    ok;

add(_P, _Id, DT) when is_integer(DT), DT=<0 ->
    lager:info("Skipping past time ~p", [DT]),
    ok;

add(P, Id, DT) when is_integer(DT), DT>=?MAXTMEM ->
    T=erlang:system_time(seconds),
    gen_server:cast(lq_w, {P, Id, T+DT, true}),
    ok;

add(P, Id, DT) when is_integer(DT), DT>0, DT<?MAXTMEM ->
    T=erlang:system_time(seconds),
    ets:insert(tasks, {{P, Id}, T+DT, true}),
    lager:info("Add ~p/~p: ~p+~p", [P, Id, T, DT]),
    ok.

del(P, Id) when not is_integer(P); not is_binary(Id) -> error;

del(P, Id) ->
    T=erlang:system_time(seconds),
    gen_server:cast(lq_w, {P, Id, T-1, false}),
    ets:insert(tasks, {{P, Id}, T-1, false}),
    ok.

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------
init(Args) ->
    {ok, Args}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

%% ets:tab2list(tasks).
%% lq_in:add(1,<<"1">>,1), lq_in:add(2,<<"1">>,5).
%% lq_in:add(1,<<"1">>,5), lq_in:add(2,<<"1">>,1).
%% lq_in:add(1,<<"1">>,{{2015,12,31},{23,59,59}}), lq_in:add(3,<<"1">>,{{2016,1,31},{23,59,59}}), ets:tab2list(tasks).
